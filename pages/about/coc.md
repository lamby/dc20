---
name: Code of Conduct
---
# Code of Conduct

DebConf is committed to a safe environment for all participants.
All attendees are expected to treat all people and facilities with respect
and help create a welcoming environment. All participants at DebConf must
follow both the
[DebConf Code of Conduct](https://debconf.org/codeofconduct.shtml) and the
[Debian Code of Conduct](https://www.debian.org/code_of_conduct).

If you notice behaviour that fails to meet this standard, please speak up and
help to keep DebConf as respectful as we expect it to be. If you are harassed
and requests to stop are not successful, or notice a disrespectful environment,
the organizers want to help.

You can contact the community team at [community@debian.org][].

We will treat your request with dignity and confidentiality, investigate, and
take whatever actions appropriate. We can provide information on security,
emergency services, transportation, alternative accommodations, or whatever
else may be necessary.

Your security and well-being is our priority, if our intervention is not
successful, DebConf reserves the right to to take action against those who do
not cease unacceptable behaviour.

[community@debian.org]: mailto:community@debian.org
