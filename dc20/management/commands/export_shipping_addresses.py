# -*- coding: utf-8 -*-
from csv import DictWriter

from django.core.management.base import BaseCommand

from register.models import Attendee


class Command(BaseCommand):
    help = 'Export shipping addresses as CSV'

    def handle(self, *args, **options):
        fields = ('username', 'name', 't_shirt_size', 'shipping_address',
                  'contact_number', 'country', 'notes')
        writer = DictWriter(self.stdout, fields)
        writer.writeheader()

        attendees = Attendee.objects.filter(completed_register_steps=8
            ).exclude(t_shirt_size=''
            ).exclude(shipping_address='')

        for attendee in attendees:
            user = attendee.user
            userprofile = user.userprofile
            writer.writerow({
                'username': user.username,
                'name': userprofile.display_name(),
                't_shirt_size': attendee.t_shirt_size,
                'shipping_address': attendee.shipping_address,
                'contact_number': userprofile.contact_number,
                'country': attendee.country,
                'notes': attendee.notes,
            })
